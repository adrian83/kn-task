build: 
	echo "building"
	mvn clean install

run:
	echo "building"
	java -jar target/kn-task-0.0.1.jar routes.csv routes.json
	
test: 
	echo "testing"
	mvn clean test

all: test build run