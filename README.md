# KN Task #

### Prerequisites ###
1. JDK 13+
2. Maven

### Running ###
1. Build: `mvn clean package`  
2. Running: `java -jar target/kn-task-0.0.1.jar <path-to-input-csv-file> <path-to-output-json-file>`  
	- `path-to-input-csv-file` - csv file with routes definitions 
	- `path-to-output-json-file` - file with geo representation of csv data extended with calculated 'average' route, this file can be visualized on [http://geojson.io/](http://geojson.io/) 