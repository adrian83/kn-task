package org.bitbucket.adrian83.kntask;

import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.bitbucket.adrian83.kntask.export.json.FeatureCollections.fromRoutes;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

import org.bitbucket.adrian83.kntask.export.json.FeatureCollection;
import org.bitbucket.adrian83.kntask.route.RouteMerger;
import org.bitbucket.adrian83.kntask.route.dto.Route;
import org.bitbucket.adrian83.kntask.route.dto.Routes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

public class Start {

  public static void main(String... args) throws Exception {

    System.out.println("\n");

    if (args.length < 2) {
      System.out.println("please provise paths for input and output");
      System.exit(1);
    }

    var inputPath = args[0];
    var outputPath = args[1];

    System.out.println("Reading from input...");

    List<Route> routes = readCsv(inputPath).map(Routes::fromCSV).collect(toList());

    System.out.println("Calculating...");

    Route conventionalSeba =
        routes
            .stream()
            .reduce(RouteMerger::merge)
            .orElseThrow(() -> new RuntimeException("Cannot merge Routes"));

    System.out.println("Writing to output...");

    var json = fromRoutes(routes, singletonList(conventionalSeba));

    writeJSON(outputPath, json);

    System.out.println("Done...");
    System.out.println("\n");
  }

  private static void writeJSON(String path, FeatureCollection coll) throws IOException {
    new ObjectMapper().writeValue(Paths.get(path).toFile(), coll);
  }

  public static Stream<String[]> readCsv(String filePath) {
    try {
      CSVReader reader = new CSVReader(new FileReader(filePath));
      reader.skip(1);
      return reader.readAll().stream();
    } catch (IOException | CsvException e) {
      throw new RuntimeException(e);
    }
  }
}
