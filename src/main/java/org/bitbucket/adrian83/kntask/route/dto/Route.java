package org.bitbucket.adrian83.kntask.route.dto;

import java.util.List;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class Route {

  private String id;
  private String from;
  private String to;
  private String startPort;
  private String destinationPort;
  private long duration;
  private long dataPointsCount;
  private List<GeoPoint> points;
}
