package org.bitbucket.adrian83.kntask.route.dto;

import static java.lang.Long.parseLong;

public final class Routes {

  private Routes() {}

  public static Route fromCSV(String[] parts) {
    return Route.builder()
        .id(parts[0])
        .from(parts[1])
        .to(parts[2])
        .startPort(parts[3])
        .destinationPort(parts[4])
        .duration(parseLong(parts[5]))
        .dataPointsCount(parseLong(parts[6]))
        .points(GeoPoints.fromCSV(parts[7]))
        .build();
  }
}
