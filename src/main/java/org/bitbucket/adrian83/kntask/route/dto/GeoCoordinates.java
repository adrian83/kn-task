package org.bitbucket.adrian83.kntask.route.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Builder
@Data
@ToString
public class GeoCoordinates {

  private double longitude;
  private double latitude;
}
