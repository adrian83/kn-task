package org.bitbucket.adrian83.kntask.route.dto;

import static java.lang.Double.parseDouble;
import static java.lang.Long.parseLong;
import static java.util.Arrays.stream;
import static java.util.Optional.of;
import static java.util.Optional.ofNullable;
import static java.util.function.Predicate.not;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class GeoPoints {

  private static final String NULL_STRING = "null";
  private static final int DEFAULT_WEIGHT = 1;

  private static final String CSV_SEPARATOR = ", ";
  private static final String CSV_POINTS_SEPARATOR = "\\], \\[";
  private static final String DOUBLE_LT_SQ_BRACKETS = "[[";
  private static final String DOUBLE_RT_SQ_BRACKETS = "]]";

  private GeoPoints() {}

  public static List<GeoPoint> fromCSV(String text) {

    var newValue =
        (text.startsWith(DOUBLE_LT_SQ_BRACKETS) && text.endsWith(DOUBLE_RT_SQ_BRACKETS))
            ? text.substring(
                DOUBLE_LT_SQ_BRACKETS.length(), text.length() - DOUBLE_RT_SQ_BRACKETS.length())
            : text;

    return stream(newValue.split(CSV_POINTS_SEPARATOR))
        .map(GeoPoints::singleFromCSV)
        .collect(Collectors.toList());
  }

  public static GeoPoint singleFromCSV(String value) {
    String[] parts = value.split(CSV_SEPARATOR);
    if (parts.length != 4) {
      throw new IllegalArgumentException("geo point should contain 4 values: " + value);
    }

    var coordinates =
        GeoCoordinates.builder()
            .latitude(parseDouble(parts[0]))
            .longitude(parseDouble(parts[1]))
            .build();

    return GeoPoint.builder()
        .coordinates(coordinates)
        .timestamp(of(parseLong(parts[2])))
        .speed(parseSpeed(parts[3]))
        .weight(DEFAULT_WEIGHT)
        .build();
  }

  private static Optional<Double> parseSpeed(String speedStr) {
    return ofNullable(speedStr).filter(not(NULL_STRING::equalsIgnoreCase)).map(Double::parseDouble);
  }
}
