package org.bitbucket.adrian83.kntask.route.dto;

import java.util.Optional;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class GeoPoint {

  private GeoCoordinates coordinates;
  private Optional<Double> speed;
  private Optional<Long> timestamp;
  private int weight;
}
