package org.bitbucket.adrian83.kntask.route;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import static java.util.Optional.empty;

import java.util.List;
import java.util.stream.Collectors;

import org.bitbucket.adrian83.kntask.route.dto.GeoCoordinates;
import org.bitbucket.adrian83.kntask.route.dto.GeoPoint;
import org.bitbucket.adrian83.kntask.route.dto.Route;

import lombok.Data;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

public class RouteMerger {

  public static Route merge(Route a, Route b) {
    List<GeoPoint> points =
        a.getPoints()
            .stream()
            .map(
                pointA -> {
                  var closestToPointA = findClosest(pointA, b.getPoints());
                  return PointsPair.builder().a(pointA).b(closestToPointA).build();
                })
            .map(
                pair ->
                    GeoPoint.builder()
                        .coordinates(pair.calculateWeightedCoordinates())
                        .weight(pair.weightsSum())
                        .speed(empty())
                        .timestamp(empty())
                        .build())
            .collect(Collectors.toList());

    return Route.builder()
        .dataPointsCount(a.getDataPointsCount())
        .startPort(a.getStartPort())
        .destinationPort(a.getDestinationPort())
        .duration(a.getDuration())
        .from(a.getFrom())
        .to(a.getTo())
        .id(a.getId())
        .points(points)
        .build();
  }

  private static GeoPoint findClosest(GeoPoint pointA, List<GeoPoint> candidates) {
    return candidates
        .stream()
        .map(
            pointB -> {
              var dist = distance(pointA.getCoordinates(), pointB.getCoordinates());
              return PointsWithDistance.builder().a(pointA).b(pointB).distance(dist).build();
            })
        .reduce((pp1, pp2) -> pp1.getDistance() < pp2.getDistance() ? pp1 : pp2)
        .map(PointsWithDistance::getB)
        .orElseThrow(() -> new RuntimeException("cannot find shortest path"));
  }

  private static double distance(GeoCoordinates first, GeoCoordinates second) {
    var lonDiff2 = pow(second.getLongitude() - first.getLongitude(), 2);
    var latDiff2 = pow(second.getLatitude() - first.getLatitude(), 2);
    return sqrt(lonDiff2 + latDiff2);
  }
}

@SuperBuilder
@Data
class PointsPair {
  private GeoPoint a;
  private GeoPoint b;

  public int weightsSum() {
    return a.getWeight() + b.getWeight();
  }

  public GeoCoordinates calculateWeightedCoordinates() {
    var pointACoord = a.getCoordinates();
    var pointBCoord = b.getCoordinates();

    var ratio = 1 - ((double) a.getWeight() / weightsSum());

    var lonDiff = pointBCoord.getLongitude() - pointACoord.getLongitude();
    var latDiff = pointBCoord.getLatitude() - pointACoord.getLatitude();

    var newLon = pointACoord.getLongitude() + (ratio * lonDiff);
    var newlat = pointACoord.getLatitude() + (ratio * latDiff);

    return GeoCoordinates.builder().latitude(newlat).longitude(newLon).build();
  }
}

@SuperBuilder
@Getter
class PointsWithDistance extends PointsPair {
  private double distance;
}
