package org.bitbucket.adrian83.kntask.export.json;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Geometry {

  public static final String DEFAULT_TYPE = "LineString";

  private String type;
  private List<Double[]> coordinates;
}
