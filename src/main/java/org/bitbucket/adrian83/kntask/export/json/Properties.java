package org.bitbucket.adrian83.kntask.export.json;

import java.util.Optional;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Properties {

  public static final String STROKE_RED = "red";
  public static final String STROKE_BLUE = "blue";

  public static final double DEFAULT_OPACITY = 0.3;

  private String id;
  private String from;
  private String to;
  private Optional<String> vessleId;
  private String stroke;
  private double strokeOpacity;
}
