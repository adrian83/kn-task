package org.bitbucket.adrian83.kntask.export.json;

import static java.util.Optional.empty;

import java.util.List;
import java.util.stream.Collectors;

import org.bitbucket.adrian83.kntask.route.dto.GeoPoint;
import org.bitbucket.adrian83.kntask.route.dto.Route;

public final class Features {

  public static final String DEFAULT_TYPE = "Feature";

  private Features() {}

  public static List<Feature> fromRoutes(List<Route> routes, String color) {

    return routes
        .stream()
        .map(
            route -> {
              var coordinates =
                  route
                      .getPoints()
                      .stream()
                      .map(GeoPoint::getCoordinates)
                      .map(c -> new Double[] {c.getLatitude(), c.getLongitude()})
                      .collect(Collectors.toList());

              var geometry =
                  Geometry.builder().type(Geometry.DEFAULT_TYPE).coordinates(coordinates).build();

              var props =
                  Properties.builder()
                      .id(route.getId())
                      .from(route.getStartPort())
                      .to(route.getDestinationPort())
                      .vessleId(empty())
                      .stroke(color)
                      .strokeOpacity(Properties.DEFAULT_OPACITY)
                      .build();

              return Feature.builder()
                  .type(Features.DEFAULT_TYPE)
                  .geometry(geometry)
                  .properties(props)
                  .build();
            })
        .collect(Collectors.toList());
  }
}
