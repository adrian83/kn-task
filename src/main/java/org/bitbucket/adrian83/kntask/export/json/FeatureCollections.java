package org.bitbucket.adrian83.kntask.export.json;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.adrian83.kntask.route.dto.Route;

public final class FeatureCollections {

  public static final String DEFAULT_TYPE = "FeatureCollection";

  private FeatureCollections() {}

  public static FeatureCollection fromRoutes(List<Route> red, List<Route> blue) {
    var redFeatures = Features.fromRoutes(red, Properties.STROKE_RED);
    var blueFeatures = Features.fromRoutes(blue, Properties.STROKE_BLUE);

    List<Feature> features = new ArrayList<>(redFeatures.size() + blueFeatures.size());
    features.addAll(redFeatures);
    features.addAll(blueFeatures);

    return FeatureCollection.builder()
        .type(FeatureCollections.DEFAULT_TYPE)
        .features(features)
        .build();
  }
}
