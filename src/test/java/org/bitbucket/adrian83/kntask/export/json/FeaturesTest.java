package org.bitbucket.adrian83.kntask.export.json;

import static java.util.List.of;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import org.bitbucket.adrian83.kntask.route.dto.GeoCoordinates;
import org.bitbucket.adrian83.kntask.route.dto.GeoPoint;
import org.bitbucket.adrian83.kntask.route.dto.Route;
import org.junit.jupiter.api.Test;

public class FeaturesTest {

  private GeoCoordinates coordinatesX1Y1 =
      GeoCoordinates.builder().longitude(1d).latitude(1d).build();

  private GeoCoordinates coordinatesX1Y2 =
      GeoCoordinates.builder().longitude(1d).latitude(2d).build();

  private GeoCoordinates coordinatesX1Y3 =
      GeoCoordinates.builder().longitude(1d).latitude(3d).build();

  private GeoPoint pointX1Y1 =
      GeoPoint.builder()
          .coordinates(coordinatesX1Y1)
          .weight(1)
          .speed(Optional.empty())
          .timestamp(Optional.empty())
          .build();

  private GeoPoint pointX1Y2 =
      GeoPoint.builder()
          .coordinates(coordinatesX1Y2)
          .weight(1)
          .speed(Optional.empty())
          .timestamp(Optional.empty())
          .build();

  private GeoPoint pointX1Y3 =
      GeoPoint.builder()
          .coordinates(coordinatesX1Y3)
          .weight(1)
          .speed(Optional.empty())
          .timestamp(Optional.empty())
          .build();

  private List<GeoPoint> points = of(pointX1Y1, pointX1Y2, pointX1Y3);

  private Route routeA =
      Route.builder()
          .id("abc-123")
          .from("1231")
          .to("5345")
          .startPort("London")
          .destinationPort("Gdansk")
          .dataPointsCount(points.size())
          .points(points)
          .build();

  @Test
  public void shouldTransformRouteIntoFeature() {
	  // given
	  var stroke = Properties.STROKE_RED;
	  
    // when
    List<Feature> features = Features.fromRoutes(of(routeA), stroke);

    // then
    assertEquals(1, features.size());

    var feature = features.get(0);
    assertEquals(Features.DEFAULT_TYPE, feature.getType());

    var geometry = feature.getGeometry();
    assertEquals(Geometry.DEFAULT_TYPE, geometry.getType());
    assertEquals(routeA.getPoints().size(), geometry.getCoordinates().size());

    for (int i = 0; i < routeA.getPoints().size(); i++) {
      var p = routeA.getPoints().get(i);
      var c = geometry.getCoordinates().get(i);

      assertEquals(2, c.length);
      assertEquals(p.getCoordinates().getLatitude(), c[0]);
      assertEquals(p.getCoordinates().getLongitude(), c[1]);
    }

    var properties = feature.getProperties();
    assertEquals(routeA.getId(), properties.getId());
    assertEquals(routeA.getStartPort(), properties.getFrom());
    assertEquals(routeA.getDestinationPort(), properties.getTo());
    assertEquals(stroke, properties.getStroke());
    assertEquals(Properties.DEFAULT_OPACITY, properties.getStrokeOpacity());
    assertTrue(properties.getVessleId().isEmpty());
  }
}
