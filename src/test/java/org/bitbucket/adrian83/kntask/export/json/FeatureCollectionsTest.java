package org.bitbucket.adrian83.kntask.export.json;

import static java.util.List.of;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Optional;

import org.bitbucket.adrian83.kntask.route.dto.GeoCoordinates;
import org.bitbucket.adrian83.kntask.route.dto.GeoPoint;
import org.bitbucket.adrian83.kntask.route.dto.Route;
import org.junit.jupiter.api.Test;

public class FeatureCollectionsTest {

  private GeoCoordinates coordinatesX1Y1 =
      GeoCoordinates.builder().longitude(1d).latitude(1d).build();

  private GeoCoordinates coordinatesX1Y2 =
      GeoCoordinates.builder().longitude(1d).latitude(2d).build();

  private GeoCoordinates coordinatesX1Y3 =
      GeoCoordinates.builder().longitude(1d).latitude(3d).build();

  private GeoPoint pointX1Y1 =
      GeoPoint.builder()
          .coordinates(coordinatesX1Y1)
          .weight(1)
          .speed(Optional.empty())
          .timestamp(Optional.empty())
          .build();

  private GeoPoint pointX1Y2 =
      GeoPoint.builder()
          .coordinates(coordinatesX1Y2)
          .weight(1)
          .speed(Optional.empty())
          .timestamp(Optional.empty())
          .build();

  private GeoPoint pointX1Y3 =
      GeoPoint.builder()
          .coordinates(coordinatesX1Y3)
          .weight(1)
          .speed(Optional.empty())
          .timestamp(Optional.empty())
          .build();

  private List<GeoPoint> points = of(pointX1Y1, pointX1Y2, pointX1Y3);

  private Route routeA =
      Route.builder()
          .id("abc-123")
          .from("1231")
          .to("5345")
          .startPort("London")
          .destinationPort("Gdansk")
          .dataPointsCount(points.size())
          .points(points)
          .build();

  @Test
  public void shouldTransformRoutesIntoFeatureCollection() {
    // when
    FeatureCollection collection = FeatureCollections.fromRoutes(List.of(routeA), List.of());

    // then
    assertEquals(FeatureCollections.DEFAULT_TYPE, collection.getType());
    assertEquals(1, collection.getFeatures().size());
  }
}
