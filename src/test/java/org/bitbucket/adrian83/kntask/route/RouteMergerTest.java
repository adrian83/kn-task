package org.bitbucket.adrian83.kntask.route;

import static java.util.List.of;
import static org.bitbucket.adrian83.kntask.route.RouteMerger.merge;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.bitbucket.adrian83.kntask.route.dto.GeoCoordinates;
import org.bitbucket.adrian83.kntask.route.dto.GeoPoint;
import org.bitbucket.adrian83.kntask.route.dto.Route;
import org.junit.jupiter.api.Test;

public class RouteMergerTest {

  // Coordinates
  private GeoCoordinates coordinatesX1Y1 =
      GeoCoordinates.builder().longitude(1d).latitude(1d).build();

  private GeoCoordinates coordinatesX3Y1 =
      GeoCoordinates.builder().longitude(3d).latitude(1d).build();

  private GeoCoordinates coordinatesX1Y2 =
      GeoCoordinates.builder().longitude(1d).latitude(2d).build();

  private GeoCoordinates coordinatesX3Y2 =
      GeoCoordinates.builder().longitude(3d).latitude(2d).build();

  private GeoCoordinates coordinatesX1Y3 =
      GeoCoordinates.builder().longitude(1d).latitude(3d).build();

  private GeoCoordinates coordinatesX3Y3 =
      GeoCoordinates.builder().longitude(3d).latitude(3d).build();

  private GeoCoordinates coordinatesX5Y1 =
      GeoCoordinates.builder().longitude(5d).latitude(1d).build();

  private GeoCoordinates coordinatesX5Y2 =
      GeoCoordinates.builder().longitude(5d).latitude(2d).build();

  private GeoCoordinates coordinatesX5Y3 =
      GeoCoordinates.builder().longitude(5d).latitude(3d).build();

  // Points
  private GeoPoint pointX1Y1 =
      GeoPoint.builder()
          .coordinates(coordinatesX1Y1)
          .weight(1)
          .speed(Optional.empty())
          .timestamp(Optional.empty())
          .build();

  private GeoPoint pointX3Y1 =
      GeoPoint.builder()
          .coordinates(coordinatesX3Y1)
          .weight(1)
          .speed(Optional.empty())
          .timestamp(Optional.empty())
          .build();

  private GeoPoint pointX1Y2 =
      GeoPoint.builder()
          .coordinates(coordinatesX1Y2)
          .weight(1)
          .speed(Optional.empty())
          .timestamp(Optional.empty())
          .build();

  private GeoPoint pointX3Y2 =
      GeoPoint.builder()
          .coordinates(coordinatesX3Y2)
          .weight(1)
          .speed(Optional.empty())
          .timestamp(Optional.empty())
          .build();

  private GeoPoint pointX1Y3 =
      GeoPoint.builder()
          .coordinates(coordinatesX1Y3)
          .weight(1)
          .speed(Optional.empty())
          .timestamp(Optional.empty())
          .build();

  private GeoPoint pointX3Y3 =
      GeoPoint.builder()
          .coordinates(coordinatesX3Y3)
          .weight(1)
          .speed(Optional.empty())
          .timestamp(Optional.empty())
          .build();

  private GeoPoint pointX5Y1 =
      GeoPoint.builder()
          .coordinates(coordinatesX5Y1)
          .weight(1)
          .speed(Optional.empty())
          .timestamp(Optional.empty())
          .build();

  private GeoPoint pointX5Y2 =
      GeoPoint.builder()
          .coordinates(coordinatesX5Y2)
          .weight(1)
          .speed(Optional.empty())
          .timestamp(Optional.empty())
          .build();

  private GeoPoint pointX5Y3 =
      GeoPoint.builder()
          .coordinates(coordinatesX5Y3)
          .weight(1)
          .speed(Optional.empty())
          .timestamp(Optional.empty())
          .build();

  // Routes
  private Route routeA = Route.builder().points(of(pointX1Y1, pointX1Y2, pointX1Y3)).build();

  private Route routeB = Route.builder().points(of(pointX3Y1, pointX3Y2, pointX3Y3)).build();

  private Route routeC = Route.builder().points(of(pointX5Y1, pointX5Y2, pointX5Y3)).build();

  @Test
  public void shouldCalculateAvgRoute() {
    // when
    Route avgRoute = merge(routeA, routeB);

    // then
    assertEquals(routeA.getPoints().size(), avgRoute.getPoints().size());

    var avgRouteP0Crds = avgRoute.getPoints().get(0).getCoordinates();
    assertEquals(2d, avgRouteP0Crds.getLongitude());
    assertEquals(1d, avgRouteP0Crds.getLatitude());

    var avgRouteP1Crds = avgRoute.getPoints().get(1).getCoordinates();
    assertEquals(2d, avgRouteP1Crds.getLongitude());
    assertEquals(2d, avgRouteP1Crds.getLatitude());

    var avgRouteP2Crds = avgRoute.getPoints().get(2).getCoordinates();
    assertEquals(2d, avgRouteP2Crds.getLongitude());
    assertEquals(3d, avgRouteP2Crds.getLatitude());
  }

  @Test
  public void shouldCalculateAvgRouteTakingWeightsIntoAccount() {
    // when
    Route avgRoute = merge(merge(routeA, routeB), routeC);

    // then
    assertEquals(routeA.getPoints().size(), avgRoute.getPoints().size());

    var avgRouteP0Crds = avgRoute.getPoints().get(0).getCoordinates();
    assertEquals(3d, avgRouteP0Crds.getLongitude());
    assertEquals(1d, avgRouteP0Crds.getLatitude());

    var avgRouteP1Crds = avgRoute.getPoints().get(1).getCoordinates();
    assertEquals(3d, avgRouteP1Crds.getLongitude());
    assertEquals(2d, avgRouteP1Crds.getLatitude());

    var avgRouteP2Crds = avgRoute.getPoints().get(2).getCoordinates();
    assertEquals(3d, avgRouteP2Crds.getLongitude());
    assertEquals(3d, avgRouteP2Crds.getLatitude());
  }
}
